Repositórios oficiais do programa "Onboard"

https://translations.launchpad.net/onboard/1.4

https://translations.launchpad.net/onboard/1.4/+pots/onboard/pt_BR/+translate



Traduções revisadas por marcelocripe:

https://gitlab.com/marcelocripe/teclado_virtual_onboard_pt_BR/-/blob/main/onboard_pt_BR.po?ref_type=heads

https://gitlab.com/marcelocripe/teclado_virtual_onboard_pt_BR/-/blob/main/onboard.desktop?ref_type=heads



Leia os motivos das traduções serem incompletas ou não estarem disponíveis para os usuários brasileiros do GNU/Linux neste tópico

https://answers.launchpad.net/ubuntu/+question/707919



Para utilizar o arquivo "onboard_pt_BR.po" e "onboard.desktop", inicie o Emulador de Terminal na pasta onde estão os arquivos que foram baixados e aplique os comandos.

"onboard_pt_BR.po":

Comando para converter o arquivo editável da tradução com a extensão ".po" para ".mo".

$ msgfmt onboard_pt_BR.po -o onboard.mo


Comando para renomear o arquivo antigo da tradução com a extensão ".mo" que está na pasta do idioma "pt_BR".

$ sudo mv /usr/share/locale/pt_BR/LC_MESSAGES/onboard.mo /usr/share/locale/pt_BR/LC_MESSAGES/onboard_antigo.mo


Comando para copiar o arquivo da tradução com a extensão ".mo" para a pasta do idioma "pt_BR".

$ sudo cp onboard.mo /usr/share/locale/pt_BR/LC_MESSAGES


"onboard.desktop":

Comando para copiar o arquivo com a extensão ".desktop" para a pasta /usr/share/applications.

$ sudo cp onboard.desktop /usr/share/applications

Comando para escrever globalmente todas as entradas dos menus do antiX:

$ sudo desktop-menu --write-out-global
